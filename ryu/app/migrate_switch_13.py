# Copyright (C) 2011 Nippon Telegraph and Telephone Corporation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ether_types
from ryu.lib.packet import in_proto
from ryu.lib.packet import arp
from ryu.lib.packet import ipv4
from ryu.lib.packet import udp
from ryu.ofproto.ofproto_v1_2 import OFPG_ANY

from ryu.app.wsgi import ControllerBase
from ryu.app.wsgi import Response
from ryu.app.wsgi import route
from ryu.app.wsgi import WSGIApplication


import configparser
import json

API_NAME = 'migrate'

HIGH_PRIO = 200  # Migrate High Priority flows
MID_PRIO  = 100  # Migrate Medium Priority flows
LOW_PRIO  = 0    # Migrate Low Priority flows

IDLE_TIMEOUT = 60

class MigrateSwitch13(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    _CONTEXTS = { 'wsgi': WSGIApplication}

    def __init__(self, *args, **kwargs):
        super(MigrateSwitch13, self).__init__(*args, **kwargs)

        wsgi = kwargs['wsgi']
        wsgi.register(RestMigrateController, {'app_instance_name': self})

        self.mac_to_port = {}
        # READ Configuration
        parser = configparser.ConfigParser()
        parser.read("migrate.ini")
        self.macmode = 0
        self.datapath = None
        self.scenario = parser['DEFAULT']['scenario']
        self.md_mac = parser[self.scenario]['md_mac']
        self.md_ip = parser[self.scenario]['md_ip']
        self.wireless_ports = json.loads(parser[self.scenario]['wireless_ports'])
        self.post_port = int(parser[self.scenario]['post_port'])
        self.coap_port = int(parser[self.scenario]['coap_port'])
        self.fake_ip = parser[self.scenario]['fake_ip']
        self.fake_mac = parser[self.scenario]['fake_mac']
        self.vim1_port = int(parser[self.scenario]['vim1_port'])
        self.vim1_mac = parser[self.scenario]['vim1_mac']
        self.vim1_ip = parser[self.scenario]['vim1_ip']
        self.vim2_port = int(parser[self.scenario]['vim2_port'])
        self.vim2_mac = parser[self.scenario]['vim2_mac']
        self.vim2_ip = parser[self.scenario]['vim2_ip']


        self.selected_vim = 1
        self.selected_vim_port = self.vim1_port

    def remove_table_flows(self, datapath, table_id, match, instructions):
        """Create OFP flow mod message to remove flows from table."""
        ofproto = datapath.ofproto
        flow_mod = datapath.ofproto_parser.OFPFlowMod(datapath, 0, 0,
                                                          table_id,
                                                          ofproto.OFPFC_DELETE,
                                                          0, 0,
                                                          1,
                                                          ofproto.OFPCML_NO_BUFFER,
                                                          ofproto.OFPP_ANY,
                                                          OFPG_ANY, 0,
                                                          match, instructions)
        return flow_mod

    def clear_switch(self, datapath, parser):
        empty_match = parser.OFPMatch()
        instructions = []
        flow_mod = self.remove_table_flows(datapath, 0,
                                           empty_match, instructions)
        print("deleting all flow entries in table ", 0)
        datapath.send_msg(flow_mod)

    # Creates the flows allowing HTTP POST messages to the manager
    def migrate_signaling_rest_flows_upward(self, datapath, parser):
        print("Installing Migrate HTTP REST flows")
        # Allow HTTP
        match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_IP, eth_src=self.md_mac,
                                ip_proto=in_proto.IPPROTO_TCP ,tcp_dst=self.post_port)
        actions = [parser.OFPActionOutput(port=self.vim1_port)]
        inst = [parser.OFPInstructionActions(datapath.ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        mod = parser.OFPFlowMod(datapath=datapath, cookie=0xcafe8080, command=ofproto_v1_3.OFPFC_ADD,
                                    idle_timeout=0, priority=MID_PRIO,
                                    match=match, instructions=inst)
        datapath.send_msg(mod)

    # Creates the lows allowing HTTP POST responses from the manager
    # NOTE: This depends on the MD port so it can not be programmed until the MD is detected,
    # and must be updated when moving
    def migrate_signaling_rest_flows_downward(self, datapath, parser):
        print("Installing Migrate HTTP REST flows")
        # Allow HTTP
        match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_IP, eth_dst=self.md_mac,
                                ip_proto=in_proto.IPPROTO_TCP, tcp_src=self.post_port)
        actions = [parser.OFPActionOutput(port=self.mac_to_port[datapath.id][self.md_mac])]
        inst = [parser.OFPInstructionActions(datapath.ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        mod = parser.OFPFlowMod(datapath=datapath, cookie=0xcafe8080, command=ofproto_v1_3.OFPFC_ADD,
                                idle_timeout=0, priority=MID_PRIO,
                                match=match, instructions=inst)
        datapath.send_msg(mod)

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        self.clear_switch(datapath, parser)

        # install table-miss flow entry
        #
        # We specify NO BUFFER to max_len of the output action due to
        # OVS bug. At this moment, if we specify a lesser number, e.g.,
        # 128, OVS will send Packet-In with invalid buffer_id and
        # truncated packet data. In that case, we cannot output packets
        # correctly.  The bug has been fixed in OVS v2.1.0.
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions)
        self.migrate_signaling_rest_flows_upward(datapath, parser)
        # We store the datapath to simplify the REST
        self.datapath = datapath

    def _send_packet(self, datapath, port, packetdata):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        self.logger.info("packet-out in port %s" % (port,))
        actions = [parser.OFPActionOutput(port=port)]
        out = parser.OFPPacketOut(datapath=datapath,
                                  buffer_id=ofproto.OFP_NO_BUFFER,
                                  in_port=ofproto.OFPP_CONTROLLER,
                                  actions=actions,
                                  data=packetdata)
        datapath.send_msg(out)

    def del_flow(self, datapath, match):
        ofproto = datapath.ofproto

        parser = datapath.ofproto_parser
        mod = parser.OFPFlowMod(datapath=datapath,
                                command=ofproto.OFPFC_DELETE,
                                out_port=ofproto.OFPP_ANY,
                                out_group=ofproto.OFPG_ANY,
                                match=match)
        datapath.send_msg(mod)

    def add_flow(self, datapath, priority, match, actions, buffer_id=None, cookie=0x00):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(cookie=cookie, datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=inst)
        else:
            mod = parser.OFPFlowMod(cookie=cookie, datapath=datapath, priority=priority,
                                    match=match, instructions=inst)
        datapath.send_msg(mod)

    def add_arp_icmp_host(self, pkt, src, dst, dpid, parser, ofproto, datapath, msg, eth, ip, in_port):
        if eth.ethertype == ether_types.ETH_TYPE_ARP:
            arpp = pkt.get_protocols(arp.arp)[0]
            if arpp.opcode == arp.ARP_REQUEST:
                print("ARP REQUEST")
                print(arpp)
            match = parser.OFPMatch(in_port=in_port, eth_dst=dst, eth_src=src, eth_type=ether_types.ETH_TYPE_ARP)
        if eth.ethertype == ether_types.ETH_TYPE_IP and ip.proto == in_proto.IPPROTO_ICMP:
            print("ICMP Message")
            match = parser.OFPMatch(in_port=in_port, eth_dst=dst, eth_src=src, eth_type=ether_types.ETH_TYPE_IP,
                                    ip_proto=in_proto.IPPROTO_ICMP)

        if dst in self.mac_to_port[dpid]:
            out_port = self.mac_to_port[dpid][dst]
        else:
            out_port = ofproto.OFPP_FLOOD

        # Backward path
        outmatch = parser.OFPMatch(in_port=out_port, eth_dst=src, eth_src=dst, eth_type=ether_types.ETH_TYPE_IP,
                                   ip_proto=in_proto.IPPROTO_ICMP)

        actions = [parser.OFPActionOutput(out_port)]
        outactions = [parser.OFPActionOutput(in_port)]

        # install a flow to avoid packet_in next time
        if out_port != ofproto.OFPP_FLOOD:
            ## match = parser.OFPMatch(in_port=in_port, eth_dst=dst, eth_src=src)
            # verify if we have a valid buffer_id, if yes avoid to send both
            # flow_mod & packet_out
            if msg.buffer_id != ofproto.OFP_NO_BUFFER:
                self.add_flow(datapath, 1, match, actions, msg.buffer_id, cookie=0xba5e)
                self.add_flow(datapath, 1, outmatch, outactions, msg.buffer_id, cookie=0xba5e)
                return
            else:
                self.add_flow(datapath, 1, match, actions, cookie=0xba5e)
                self.add_flow(datapath, 1, outmatch, outactions, cookie=0xba5e)
        data = None
        if msg.buffer_id == ofproto.OFP_NO_BUFFER:
            data = msg.data

        out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,
                                  in_port=in_port, actions=actions, data=data)
        datapath.send_msg(out)

    def delete_arp_icmp_host(self, pkt, src, dst, dpid, parser, ofproto, datapath, msg, eth, ip, in_port):
        match = parser.OFPMatch(eth_src=src, eth_type=ether_types.ETH_TYPE_ARP)
        # self.del_flow(datapath, 1, match, None, cookie=0xba5e)
        self.del_flow(datapath, match)

        match = parser.OFPMatch(eth_dst=src, eth_type=ether_types.ETH_TYPE_ARP)
        # self.del_flow(datapath, 1, match, None, cookie=0xba5e)
        self.del_flow(datapath, match)

        match = parser.OFPMatch(eth_src=src, eth_type=ether_types.ETH_TYPE_IP,
                                ip_proto=in_proto.IPPROTO_ICMP)
        # self.del_flow(datapath, 1, match, None, cookie=0xba5e)
        self.del_flow(datapath, match)
        match = parser.OFPMatch(eth_dst=src, eth_type=ether_types.ETH_TYPE_IP,
                                ip_proto=in_proto.IPPROTO_ICMP)
        # self.del_flow(datapath, 1, match, None, cookie=0xba5e)
        self.del_flow(datapath, match)

    def generateArpResponse(self, src, src_ip):
        pkt = packet.Packet()
        ethp = ethernet.ethernet(ethertype=ether_types.ETH_TYPE_ARP,
                                           dst=src,
                                           src=self.fake_mac)
        pkt.add_protocol(ethp)
        arpp = arp.arp_ip(opcode=arp.ARP_REPLY,
                                 src_mac=self.fake_mac, src_ip=self.fake_ip,
                                 dst_mac=src, dst_ip=src_ip)
        pkt.add_protocol(arpp)

        ethp._MIN_PAYLOAD_LEN = 28 # This is maybe not needed but if not a unnecesary payload is generated
        print("pkt len: {}".format(pkt.__len__()))
        pkt.serialize()
        print(pkt)
        bin_packet = pkt.data
        print("data len {}".format(len(pkt.data)))
        return bin_packet

    # Creates the flows that allos CoAP communication both directions to the selected VIM
    def update_selected_vim(self):
        print("update_selected_vim to {}".format(self.selected_vim))
        parser = self.datapath.ofproto_parser

        if self.selected_vim == 1:
            self.selected_vim_port = self.vim1_port
        else:
            self.selected_vim_port = self.vim2_port

        # Remove existing CoAP Flows
        match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_IP, ip_proto=in_proto.IPPROTO_UDP, udp_dst=self.coap_port)
        self.del_flow(datapath=self.datapath, match=match)

        # Create upward CoAP flow
        if self.macmode:
            match = parser.OFPMatch(in_port=self.mac_to_port[self.datapath.id][self.md_mac], eth_type=ether_types.ETH_TYPE_IP, eth_src=self.md_mac,
                                ip_proto=in_proto.IPPROTO_UDP, udp_dst=self.coap_port)
        else:
            match = parser.OFPMatch(in_port=self.mac_to_port[self.datapath.id][self.md_mac],eth_type=ether_types.ETH_TYPE_IP,
                                    ip_proto=in_proto.IPPROTO_UDP, udp_dst=self.coap_port)
        print("UPward match: {}".format(match))
        if self.selected_vim == 1:
            # VIM1
            actions = [parser.OFPActionSetField(eth_src=self.fake_mac),
                       parser.OFPActionSetField(ipv4_src=self.fake_ip),
                       parser.OFPActionSetField(eth_dst=self.vim1_mac),
                       parser.OFPActionSetField(ipv4_dst=self.vim1_ip),
                       parser.OFPActionOutput(port=self.vim1_port)]
        elif self.selected_vim == 2:
            # VIM2
            actions = [parser.OFPActionSetField(eth_src=self.fake_mac),
                       parser.OFPActionSetField(ipv4_src=self.fake_ip),
                       parser.OFPActionSetField(eth_dst=self.vim2_mac),
                       parser.OFPActionSetField(ipv4_dst=self.vim2_ip),
                       parser.OFPActionOutput(port=self.vim2_port)]
        else:
            raise("VIM not supported: {}".format(self.selected_vim))

        print("UPward actions: {}".format(actions))
        inst = [parser.OFPInstructionActions(self.datapath.ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        mod = parser.OFPFlowMod(datapath=self.datapath, cookie=0xcafe, command=ofproto_v1_3.OFPFC_ADD,
                                idle_timeout=0, priority=MID_PRIO,
                                match=match, instructions=inst)
        self.datapath.send_msg(mod)

        # Downward CoAP flow
        match = None
        actions = None

        if self.selected_vim == 1:
            # VIM1
            match = parser.OFPMatch(in_port=self.vim1_port, eth_type=ether_types.ETH_TYPE_IP,
                                    eth_dst=self.fake_mac,
                                    ip_proto=in_proto.IPPROTO_UDP, udp_dst=self.coap_port)
            actions = [parser.OFPActionSetField(eth_src=self.fake_mac),
                       parser.OFPActionSetField(ipv4_src=self.fake_ip),
                       parser.OFPActionSetField(eth_dst=self.md_mac),
                       parser.OFPActionSetField(ipv4_dst=self.md_ip),
                       parser.OFPActionOutput(port=self.mac_to_port[self.datapath.id][self.md_mac])]
        elif self.selected_vim == 2:
            # VIM2
            match = parser.OFPMatch(in_port=self.vim2_port, eth_type=ether_types.ETH_TYPE_IP,
                                    eth_dst=self.fake_mac,
                                    ip_proto=in_proto.IPPROTO_UDP, udp_dst=self.coap_port)
            actions = [parser.OFPActionSetField(eth_src=self.fake_mac),
                       parser.OFPActionSetField(ipv4_src=self.fake_ip),
                       parser.OFPActionSetField(eth_dst=self.md_mac),
                       parser.OFPActionSetField(ipv4_dst=self.md_ip),
                       parser.OFPActionOutput(port=self.mac_to_port[self.datapath.id][self.md_mac])]
        else:
            raise("VIM not supported: {}".format(self.selected_vim))

        print("DOWNward match: {}".format(match))
        print("DOWNward actions: {}".format(actions))

        inst = [parser.OFPInstructionActions(self.datapath.ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        mod = parser.OFPFlowMod(datapath=self.datapath, cookie=0xcafe, command=ofproto_v1_3.OFPFC_ADD,
                            idle_timeout=0, priority=MID_PRIO,
                            match=match, instructions=inst)
        self.datapath.send_msg(mod)

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        # If you hit this you might want to increase
        # the "miss_send_length" of your switch
        if ev.msg.msg_len < ev.msg.total_len:
            self.logger.debug("packet truncated: only %s of %s bytes",
                              ev.msg.msg_len, ev.msg.total_len)
        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]

        if eth.ethertype == ether_types.ETH_TYPE_LLDP:
            # ignore lldp packet
            return

        # Extract IP header if present
        ip = None
        if eth.ethertype == ether_types.ETH_TYPE_IP:
            ip = pkt.get_protocols(ipv4.ipv4)[0]

        dst = eth.dst
        src = eth.src

        dpid = datapath.id
        self.mac_to_port.setdefault(dpid, {})

        self.logger.info("packet in %s %s %s %s", dpid, src, dst, in_port)
        self.logger.info("eth %s", eth)
        if ip != None:
            self.logger.info("IP %s", ip)

        ###################################
        ## MACMODE True -> Movement detected via mac ##
        ## MACMODE False -> Movement detected with destination IP and Port
        ###################################
        if self.macmode and src in self.mac_to_port[dpid] and src == self.md_mac and self.mac_to_port[dpid][src] != in_port:
            ###################################
            ## HOST MOVED. UPDATE BASE FLOWS ##
            ###################################
            print("{} has moved from port {} to port {}".
                  format(src, self.mac_to_port[dpid][src], in_port))
            self.delete_arp_icmp_host(pkt, src, dst, dpid, parser, ofproto, datapath, msg, eth, ip, self.mac_to_port[dpid][src])
            del self.mac_to_port[dpid][src]
        elif not self.macmode and \
                in_port in self.wireless_ports and\
                ip != None and\
                ip.dst==self.fake_ip : # It is an IP packet directed to the VMD
            print("IP to VMD")
            print(str(ip.proto) +" vs " + str(in_proto.IPPROTO_UDP))
            # Check that it is udp and the port is ok
            if ip.proto == in_proto.IPPROTO_UDP: # The packet is UDP
                print("UDP!")
                udpp = pkt.get_protocols(udp.udp)[0]
                print(udpp)
                if udpp.dst_port == self.coap_port: # The port is COAP
                    print("COAP!!!")
                    if self.md_mac != eth.src:
                        print("{} with mac {} has moved from port {} to port {} with mac {}".
                              format(ip.src, self.md_mac, self.mac_to_port[dpid][self.md_mac], in_port, eth.src))
                        #self.delete_arp_icmp_host(pkt, eth.src, dst, dpid, parser, ofproto, datapath, msg, eth, ip,
                        #                          self.mac_to_port[dpid][src])
                        self.md_mac = eth.src

                        #del self.mac_to_port[dpid][src]
                        self.update_selected_vim()


        # learn a mac address to avoid FLOOD next time.
        if not src in self.mac_to_port[dpid]:
            self.mac_to_port[dpid][src] = in_port
            if src == self.md_mac:
                self.migrate_signaling_rest_flows_downward(datapath, parser)
                self.update_selected_vim()

        print("Looking for {} in switch {} with macs: {}".format(dst, dpid, self.mac_to_port[dpid]))

        # ALLOW ICMP and ARP
        if eth.ethertype == ether_types.ETH_TYPE_ARP or \
                (eth.ethertype == ether_types.ETH_TYPE_IP and ip.proto == in_proto.IPPROTO_ICMP):

            ########################
            # FAKE IP/MAC HANDLING #
            ########################
            if eth.ethertype == ether_types.ETH_TYPE_ARP:
                arpp = pkt.get_protocols(arp.arp)[0]
                if arpp.opcode == arp.ARP_REQUEST and arpp.dst_ip == self.fake_ip:
                    print("Resolving Fake ARP")
                    arppacket = self.generateArpResponse(arpp.src_mac, arpp.src_ip)
                    self._send_packet(datapath, in_port, arppacket)
                    return
            ########################
            # END FAKE IP HANDLING #
            ########################

            self.add_arp_icmp_host(pkt,src,dst,dpid,parser,ofproto,datapath,msg,eth,ip,in_port)
        # ICMP AND ARP TREATMENT ENDS


######
#REST#
######

class RestMigrateController(ControllerBase):
    VIM_PATTERN=r'[1-2]'
    def __init__(self, req, link, data, **config):
        super(RestMigrateController, self).__init__(req, link, data, **config)
        self.app_instance_name = data['app_instance_name']
        print("RESTController: Previously selected VIM: {}".format(self.app_instance_name.selected_vim))

    @route(API_NAME, '/getConfig', methods=['GET'])
    def get_config(self, req, **kwargs):
        print("RESTController: Config requested")
        return Response(status=405)

    @route(API_NAME, '/setVIM/{vim}', methods=['GET'], requirements={'vim': VIM_PATTERN})
    def get_config(self, req, **kwargs):
        if 'vim' in kwargs:
            print("RESTController: setting VIM to {}".format(kwargs['vim']))
            self.app_instance_name.selected_vim = int(kwargs['vim'])
            self.app_instance_name.update_selected_vim()
            return Response(status=200)
        else:
            print("")
            return Response(status=500,
                            body="VIM must be in format: {}".format(self.VIM_PATTERN))
